<!DOCTYPE html>
<html lang="en">
    <head>

        <!-- start: Meta -->
        <meta charset="utf-8">
        <title>E-Shop Login</title>
        <link id="bootstrap-style" href="<?php echo base_url()?>assets/admin/css/bootstrap.min.css" rel="stylesheet">
        <link id="base-style" href="<?php echo base_url()?>assets/admin/css/style.css" rel="stylesheet">
     
     
    </head>

    <body>
        <div class="container-fluid-full">
            <div class="row-fluid">

                <div class="row-fluid">
                    <div class="login-box">
                        <h2>Login to your account</h2>
                        <style type="text/css">
                            #result{color:red}
                            #result p{color:red}
                        </style>
                        <div id="result">
                            <p><?php echo $this->session->flashdata('message');?></p>
                        </div>
                        <form id="adminlogincheck" class="form-horizontal" action="<?php echo base_url()?>admin_login_check" method="post">
                            <fieldset>

                                <div class="input-prepend" title="User Email">
                                    <span class="add-on"><i class="halflings-icon user"></i></span>
                                    <input class="input-large span10" value="<?php set_value('user_name');?>" name="user_email" id="user_email" type="text" placeholder="type useremail"/>
                                </div>
                                <div class="clearfix"></div>

                                <div class="input-prepend" title="User Password">
                                    <span class="add-on"><i class="halflings-icon lock"></i></span>
                                    <input class="input-large span10" name="user_password" id="user_password" type="password" placeholder="type password"/>
                                </div>
                                <div class="clearfix"></div>
                                <div class="button-login">	
                                    <button type="submit" class="btn btn-primary adminlogincheck">Login</button>
                                </div>
                                <div class="clearfix"></div>
                                                          </fieldset>
  
                        </form>
                       	
                    </div><!--/span-->
                </div><!--/row-->


            </div><!--/.fluid-container-->

        </div><!--/fluid-row-->

        

    </body>
</html>
