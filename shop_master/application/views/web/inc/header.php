<!DOCTYPE HTML>
<head>
    <title>Shop Master</title>
    <link href="<?php echo base_url() ?>assets/web/css/style.css" rel="stylesheet" type="text/css" media="all"/>
    <link href="<?php echo base_url() ?>assets/web/css/menu.css" rel="stylesheet" type="text/css" media="all"/>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/web/js/jquery-1.7.2.min.js"></script> 
</head>
<body>
    <div class="wrap">
        <div class="header_top">
          
            <div class="header_top_right">
                
                <div class="shopping_cart">
                    <div class="cart">
                        <a href="<?php echo base_url('cart');?>" title="View my shopping cart" rel="nofollow">
                            <span class="cart_title">Cart</span>
                            <span class="no_product">(<?php echo $this->cart->total_items();?> Items)</span>
                        </a>
                    </div>
                </div>
                <?php
                $customer_id = $this->session->userdata('customer_id');
                if ($customer_id) {
                    ?>
                    <div class="login"><a href="<?php echo base_url('/customer/logout'); ?>">Logout</a></div>
                <?php } else {
                    ?>
                    <div class="login"><a href="<?php echo base_url('/customer/login'); ?>">Login</a></div>
                    <div class="login"><a href="<?php echo base_url('/customer/register'); ?>">Register</a></div>

                    <?php
                }
                ?>
                <div class="clear"></div>
            </div>
            <div class="clear"></div>
        </div>
        <div class="menu">
            <ul id="dc_mega-menu-orange" style="float:left" class="dc_mm-orange">
                <li class=""><a href="<?php echo base_url('/product'); ?>">Products</a> </li>
                  
            </ul>
            <div class="clear"></div>
        </div>
<style>
   .login { width: 223px;!important}
   .header_top_right {
    float: right;
    width: 65%;
    margin-top: 35px;
}
</style>