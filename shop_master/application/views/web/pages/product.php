

<div class="main">
    <div class="content">
        <div class="content_top">
            <div class="heading">
                <h3>Shop Page</h3>
            </div>
            <div class="clear"></div>
        </div>

        <?php
        $arr_chunk_product = array_chunk($get_all_product, 4);

        foreach ($arr_chunk_product as $chunk_products) {
            ?>
            <div class="section group">
                <?php foreach ($chunk_products as $single_products) { ?>
                    <div class="grid_1_of_4 images_1_of_4">
                        <a href="<?php echo base_url('single/'.$single_products->product_id);?>"><img style="width:250px;height:250px" src="<?php echo base_url('uploads/'.$single_products->product_image)?>" alt="" /></a>
                        <h2><?php echo $single_products->product_title ?></h2>
                        <p><?php echo word_limiter($single_products->product_short_description, 10) ?></p>
                        <p><span class="price">Rs. <?php echo $this->cart->format_number($single_products->product_price) ?> </span></p>
                        <div class="button"><span><a href="<?php echo base_url('single/'.$single_products->product_id);?>" class="details">View Details</a></span></div>
                        <div class="add-cart">
                        <form action="<?php echo base_url('save/cart');?>" method="post">
                            <input type="number" class="buyfield" name="qty" value="1"/>
                            <input type="hidden" class="buyfield" name="product_id" value="<?php echo $single_products->product_id?>"/>
                            <input type="submit" class="" name="submit" value="Add to Cart"/>
                        </form>             
                    </div>
                    </div>
                    <?php
                }
                ?>

            </div>
            <?php
        }
        ?>
    </div>
</div>