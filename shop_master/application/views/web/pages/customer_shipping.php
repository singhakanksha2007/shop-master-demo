<?php //echo '<pre>';print_r($result);?>
<!-- stdClass Object
(
    [customer_id] => 9
    [customer_name] => test
    [customer_email] => test@gmail.com
    [customer_password] => e10adc3949ba59abbe56e057f20f883e
    [customer_address] => 
    [customer_city] => 
    [customer_zipcode] => 
    [customer_phone] => 99999999
    [customer_country] => 
    [customer_active] => 1
) -->

<div class="main">
    <div class="content" style="text-align: center">
        <div class="register_account" style="text-align:center;display:inline-block;float: none">
            <h3>Your Shipping Address</h3>
            <style type="text/css">
                #result{color:red;padding: 5px}
                #result p{color:red}
            </style>
            <div id="result">
                <p><?php echo $this->session->flashdata('message'); ?></p>
            </div>
             <?php 
                $name = !empty($result->customer_name) ? $result->customer_name: '';
                $customer_city = !empty($result->customer_city) ? $result->customer_city: '';
                $shipping_phone = !empty($result->customer_phone) ? $result->customer_phone: '';
                $shipping_zipcode = !empty($result->customer_zipcode) ? $result->customer_zipcode: '';
                $shipping_email = !empty($result->customer_email) ? $result->customer_email: '';
                $shipping_address = !empty($result->customer_address) ? $result->customer_address: '';
                $customer_country = !empty($result->customer_country) ? $result->customer_country: '';
                ?>
            <form method="post" action="<?php echo base_url('customer/save/shipping/address');?>">
               
                <table>
                    <tbody>
                        <tr>
                            <td>
                                <div>
                                    <input type="text" value="<?php echo $name;?>" name="shipping_name" placeholder="Enter Your Name">
                                </div>


                                <div>
                                    <input type="text" value="<?php echo $customer_city;?>" name="shipping_city" placeholder="Enter Your City">
                                </div>
                                <div>
                                    <input type="text" value="<?php echo $shipping_phone;?>" name="shipping_phone" placeholder="Enter Your Phone">
                                </div>
                                <div>
                                    <input type="text" value="<?php echo $shipping_zipcode;?>" name="shipping_zipcode" placeholder="Enter Your ZipCode">
                                </div>
                            </td>
                            <td>
                                <div>
                                    <input type="text" value="<?php echo $shipping_email;?>" name="shipping_email" placeholder="Enter Your Email">
                                </div>
                                        

                                <div>
                                    <input type="text" value="<?php echo $name;?>" name="shipping_address" placeholder="Enter Your Address">
                                </div>
                                
                                <div>
                                    <?php $arr =array('India', 'US');?>
                                    <select id="country" value="<?php echo $name;?>" name="shipping_country" class="frm-field required">
                                        <option value="null">Select a Country</option> 
                                        <?php foreach($arr as $con){
                                            $checked =($con==$customer_country) ? "checked=checked" :''; ?>
                                            <option  <?php echo $checked;?> value="<?php echo $con;?>"><?php echo $con;?></option>
                                       <?php }?> 

                                    </select>
                                </div>		

                                
                            </td>
                        </tr> 
                    </tbody></table> 
                <div class="search"><div><button class="grey">Proceed</button></div></div>
                <div class="clear"></div>
            </form>
        </div>  	
        <div class="clear"></div>
    </div>
</div>